---
title: "cvxLogReg"
author: "Charles"
date: "July 22, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librairies
 
```{r  }
 
 
library(GeneralHelpers)
GeneralHelpers::loadAll()
library(CVXR)
library(quantreg)
library(ompr)
library(ROI.plugin.glpk)
library(ompr.roi)
 
library(caret)
library(glue)
 

``` 



#Load the data
```{r}

dfDA <- read_csv( file.path(here::here("Data", "Csv", "shpDAWithMinoritiesIncome.csv" ) ) )
dfDA %>% head

```

#Data prep

##Data cleaning

```{r}

useBinaryCityResp <- T
listSplit <- readMinorityDA(useBinaryCityResp=useBinaryCityResp)
respstr <- ifelse(useBinaryCityResp, "region.Québec", "medianTotalIncome")

X <- listSplit[["X"]][['Train']]
y <- listSplit[["y"]][['Train']] 

X %<>% dplyr::select(-one_of(c("region.Québec", "medianTotalIncome") ) ) #make sure no response in the design matrix

X %>% head

print( colnames(X) )

```
 
 


 

#Regularized logistic regression with cvx

```{r}

lambda <- 10

solutionCVXLogRegPen10 <- fitLogRegL1(X,y,lambda) %>% round(digits = 3) 

print(glue("Here is the solution with CVX + L1 regularized logistic regression (lambda:{lambda})"))


dfCVXLambda10 <- getCVXSolutionWithVarNames(X,solutionCVXLogRegPen10)

print(dfCVXLambda10)
```

#Glmnet implementation?
```{r}

 

# glmnetmod <- glmnet::glmnet(X %>% as.matrix ,y %>% as.factor() , 
#                     family = "binomial", 
#                     alpha=1, #only norm 1
#                     lambda = seq(0,lambda, length.out = 10)) #make sure we consider the same lambda as the one used in the manual cvx implementation

glmnetmod <- glmnet::glmnet(X %>% as.matrix ,y %>% as.factor() , 
                    family = "binomial", 
                    alpha=1, #only norm 1
                    lambda = lambda,
                    standardize=F,
                    intercept=F) #glmnet does not penalize the offset by default, while in cvxr we penalize the entire vector of coefficients
 
dfGlmNet <- predict(glmnetmod, 
                    newx = X %>% as.matrix, 
                    type = "coeff", 
                    s = which( glmnetmod$lambda == lambda )) %>%  #get the proper index corresponding to the desired lambda
  as.matrix %>% 
  as.data.frame  

dfGlmNet["var"] <- rownames(dfGlmNet)
dfGlmNet %<>% rename("valGlmNet" = "1" )


dfGlmNet %<>% filter( var != "X.Intercept..1")
dfGlmNet[ dfGlmNet$var == "X.Intercept.", "var"] <- "(Intercept)"
 
print(dfGlmNet)

```

#"Penalize" implementation
```{r}

library(penalized)
 
penmod <- penalized::penalized(y %>% as.factor() , 
                                  penalized=X %>% as.matrix ,
                                  unpenalized = ~ 0,
                                  lambda1 = lambda,
                                  lambda2 = 0)
        
dfPen <- coefficients(penmod) %>% 
  as.data.frame  ()

dfPen["var"] <- rownames(dfPen)
dfPen %<>% rename("valpenalized" = "." )

 
print(dfPen)

```

```{r}

dfCompareRegularized <- left_join(dfGlmNet, dfCVXLambda10,
          by="var")

left_join(dfCompareRegularized, dfPen,
          by="var")

```

#No regularization - cvx implementation
```{r}

lambda <- 0

solutionCVXLogReg <- fitLogRegL1(X,y,lambda)   %>% round(digits = 3) 

print(glue("Here is the solution with CVX for regularized logistic regression WITHOUT any L1 regularization"))
 

dfCvxNoPen <- getCVXSolutionWithVarNames(X,solutionCVXLogReg)

print(dfCvxNoPen)

```

#Regular logistic regression
```{r}

Xy <- X
Xy["respStr"] <- y %>% as.factor
glmFit <- glm( formula =  as.formula("  respStr ~ ."), data=Xy, family = "binomial")


dfGlm <- broom::tidy(glmFit) %>% dplyr::select(term, estimate)

dfGlm

```

#Easier comparison
```{r}

left_join(dfGlm, dfCvxNoPen,
          by=c("term"="var"))

```


#Large regularization - cvx implementation - force some coeff to 0
```{r}

lambda <- 1000

solutionCVXLogReg100 <- fitLogRegL1(X,y,lambda)   %>% round(digits = 3) 

print(glue("Here is the solution with CVX for regularized logistic regression with large L1 regularization - lambda: {lambda}"))
 

dfCvxLogReg100 <- getCVXSolutionWithVarNames(X,solutionCVXLogReg100)

print(dfCvxLogReg100)

```