
getSubgradientNN <- function(a, y=1, x=1){
  
  
  return( getSubgradientAbsVal( y-posPart(a*x) ) * -getSubgradientMaxFct(a*x) * x )
  

}


getSubgradientMaxFct <- function(u){
  
  if(u>0){
    return(1)
  }else{ return (0)}
  
}


getSubgradientNNLogistic <- function(a, y=1, x=1){
  
  
  return( -1*getSubgradientAbsVal( 1-logistic(a*x) ) * x * logistic(a*x) * (1-logistic(a*x)) )
  
  
}

getSubgradientNNLogisticPoint8X1 <- function(a, y=0.8, x=1){
  
  
  return( -1*getSubgradientAbsVal( 1-logistic(a*x) ) * x * logistic(a*x) * (1-logistic(a*x)) )
  
  
}

