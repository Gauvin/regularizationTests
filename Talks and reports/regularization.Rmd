---
title: "Regularization and sparsity"
author: "Charles"
date: "06/08/2019"
output:
  html_document:
    toc: true
    toc_float: true
    toc_depth: 2
    mathjax: local
    self_contained: false
    header-includes:
      - \usepackage{amsmath}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r librairis, include=FALSE}
library(magrittr)
library(purrr)
library(CVXR)
library(dplyr)
library(ggplot2)
library(glue)
```

#Recap of Lasso

Given parameter $$\lambda >0$$, design matrix $X \in \mathbb{R}^{n \times p}$ with response $y \in \mathbb{R}^n$, recall L1 regularized regression problem:

$$
\min_{\beta} \frac{1}{2}{\left\lVert X\beta-y \right\lVert}_2^2 + \lambda {\left\lVert  \beta  \right\lVert}_1
$$
  

We can show that the optimal solution respects the following condition:

$$
\begin{cases}
(X^{\top}X)_i \beta - X_i^{\top} y  + \lambda = 0 & \text{if } \beta_i>0 \\
-\lambda \leq (X^{\top}X)_i \beta - X_i^{\top} y  \leq \lambda = 0 & \text{if } \beta_i=0 \\
(X^{\top}X)_i \beta - X_i^{\top} y  - \lambda = 0 & \text{if } \beta_i<0
\end{cases}
$$

##A specific case for intuition
In the particular case where the design matrix is orthogonal (e.g. if the X is a one-hot encoding without intercept), we have $X^{\top}X = I$ and 

$\beta^{OLS} = X^{\top}y$ is the optimal solution of ordinary least square that solves: $\min_{\beta} \frac{1}{2}{\left\lVert X\beta-y \right\lVert}_2^2$

In that case, we see that $\beta^{Lasso} =S_{\lambda}(X^{\top}y)  $, where 

$$S_{\lambda}(x) = \begin{cases} 
x-\lambda & \text{if } x > \lambda \\
0 & \text{if } -\lambda < x < \lambda \\
x+\lambda & \text{if } x < -\lambda \\
\end{cases}
$$

is the soft thresholding operator. 

```{r}

softThresh <- function(x,lambda=1){
  
  if(x>lambda) {valRet <-  x-lambda}
  else if (x <  -lambda) { valRet <- x+lambda}
  else valRet <-  0
  
  return(valRet)
}

l <- 5
x <- seq(-10,10,length.out = 50)
dfSoftThresh <- data.frame( x=x,
                            y=map_dbl(x, ~softThresh(.x,l))
)

ggplot() + 
  geom_line(data=dfSoftThresh, aes(x=x,y=y)  ) + 
  geom_line(data=dfSoftThresh, aes(x=x,y=x), linetype = "dashed")
  ggtitle(glue("Soft thresholding operator - lambda:{l}"))

```


#General case

In general, for any regression or classification problem with loss $L_{X,y}(\beta)$ we know that a necessary condition for solving 

$$
\min_{\beta} L_{X,y}(\beta)  + \lambda {\left\lVert  \beta  \right\lVert}_1
$$

is to find a stationnary point such that:

$$ 0 \in \partial L_{X,y}(\beta)  + \lambda  \partial {\left\lVert  \beta  \right\lVert}_1  $

where $\partial f = \{g \in \mathbb{R}^p : f(x_0) + g^{\top}(x-x_0) \leq f(x) , \forall   \}$